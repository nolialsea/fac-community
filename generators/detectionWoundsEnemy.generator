{
  "name": "Wound Detection Generator",
  "description": "Generates wounds given a description of an attack",
  "aiParameters": {
    "temperature": 1,
    "repetition_penalty": 1.02
  },
  "context": "[ WOUNDS DETECTION: given a description of a attacker's attack against a player, detects the new player's wound(s). ]\n[ Note: only detects player's wounds, NOT attacker's wounds. ]\n[ Fatal wounds: if the player dies, a wound named \"fatal wound\" is added. ]",
  "properties": [
    {
      "name": "description",
      "replaceBy": "Description of the attacker's attack:",
      "input": true
    },
    {
      "name": "wounds",
      "replaceBy": "Player's new wound(s):"
    }
  ],
  "list": [
    {
      "description": "The orc attacks Arshes Skree using his bare hands. Arshes manages to dodge or block some of the attacks, but still gets bruised and tired from the efforts.",
      "wounds": "bruised, general tiredness"
    },
    {
      "description": "The troll attacks Freyja Ymir with his large wooden club, and hits Freyja right in the face. Freyja is severely injured, and she falls down before dying shortly after.",
      "wounds": "fractured skull"
    },
    {
      "description": "The minotaur uses a spear to attack Thaddeus Morgan, but Thaddeus' armor deflects the blow! Thaddeus is completely unscathed and immediately strikes back by giving a powerful punch in the minotaur's face.",
      "wounds": "none"
    },
    {
      "description": "The skeleton brandishes his scimitar and violently hits Magnus Shredmur across his torso, eviscerating him. Magnus' blood sprays everywhere and his guts fall out of his body as he feels his life force escaping.",
      "wounds": "eviscerated"
    },
    {
      "description": "The dire wolf jumps at Asherah Balek and gives a powerful bite to her right leg. Asherah is badly injured by the beast's fangs, and she bleeds heavily.",
      "wounds": "mangled right leg, heavily bleeding"
    },
    {
      "description": "The golden dragon breathes fire upon Jorel Smark, but Jorel deflects the flames with his magic shield! Jorel then slices the dragon's head right off!",
      "wounds": "none"
    },
    {
      "description": "The goblin slashes Reezea Storar across the chest with his knife. Reezea's cloth armor does nothing for protection, and she slowly starts to bleed from the cut.",
      "wounds": "slashed chest"
    },
    {
      "description": "The goblin pierces Yakhil Tern with his wooden spear. The goblin's spear flies into Yakhil's stomach and guts, disemboweling him and spreading his blood and intestines everywhere.",
      "wounds": "disemboweled stomach"
    },
    {
      "description": "The large troll swings her steel axe at Akhil Tern with strength, severing its left leg in one slash.",
      "wounds": "severed left leg"
    },
    {
      "description": "The kobold swiftly attacks Kalum Derant with his copper dagger, cutting Kalum's left ear off.",
      "wounds": "left ear cut off"
    },
    {
      "description": "The small goblin stabs Kalum Derant in the chest, pushing the blade deep through the Kalum's heart. Kalum Derant is badly wounded and doesn't even fight back.",
      "wounds": "pierced heart"
    },
    {
      "description": "The troll strikes Tasha Kanata with his wooden club, only causing light bruises on her sternum. Tasha Kanata is however badly in shape and bleeding profusely, and just crumbles to the floor few seconds later, dead.",
      "wounds": "bruised chest, bleeding profusely"
    },
    {
      "description": "The night elf fires a poisonous arrow from her elven bow at Lagana Tumana. Lagana is hit straight through her stomach and starts to lose blood, but she keeps fighting.",
      "wounds": "punctured stomach, slight blood loss"
    }
  ]
}